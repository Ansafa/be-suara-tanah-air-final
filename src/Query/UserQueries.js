const mongoose = require('mongoose');

const findUserLikedSongs = (userId) => ([
  {
    $match: {
      _id: new mongoose.Types.ObjectId(userId)
    }
  },
  {
    '$project': {
      '_id': 1, 
      'username': 1, 
      'likedMusic': 1, 
      'likedUploadMusic': 1, 
      'email': 1
    }
  }, {
    '$unwind': {
      'path': '$likedMusic', 
      'preserveNullAndEmptyArrays': true
    }
  }, {
    '$lookup': {
      'from': 'songs', 
      'localField': 'likedMusic', 
      'foreignField': '_id', 
      'as': 'likedMusic'
    }
  }, {
    '$unwind': {
      'path': '$likedMusic', 
      'preserveNullAndEmptyArrays': true
    }
  }, {
    '$group': {
      '_id': '$_id', 
      'username': {
        '$first': '$username'
      }, 
      'likedMusic': {
        '$push': '$likedMusic'
      }, 
      'email': {
        '$first': '$email'
      }, 
      'likedUploadMusic': {
        '$addToSet': '$likedUploadMusic'
      }
    }
  }, {
    '$unwind': {
      'path': '$likedUploadMusic', 
      'preserveNullAndEmptyArrays': true
    }
  }, {
    '$lookup': {
      'from': 'uploadsongs', 
      'localField': 'likedUploadMusic', 
      'foreignField': '_id', 
      'as': 'likedUploadMusic'
    }
  }, {
    '$unwind': {
      'path': '$likedUploadMusic', 
      'preserveNullAndEmptyArrays': true
    }
  }, {
    '$group': {
      '_id': '$_id', 
      'username': {
        '$first': '$username'
      }, 
      'likedMusic': {
        '$first': '$likedMusic'
      }, 
      'email': {
        '$first': '$email'
      }, 
      'likedUploadMusic': {
        '$addToSet': '$likedUploadMusic'
      }
    }
  }, {
    '$project': {
      '_id': 1, 
      'username': 1, 
      'email': 1, 
      'likedSong': {
        '$concatArrays': [
          '$likedMusic', '$likedUploadMusic'
        ]
      }
    }
  }
])

const findUserUploadedSongs = (userId) => ([
  {
    $match: {
      _id: new mongoose.Types.ObjectId(userId)
    }
  }, {
    '$unwind': {
      'path': '$uploadMusic', 
      'preserveNullAndEmptyArrays': true
    }
  }, {
    '$lookup': {
      'from': 'uploadsongs', 
      'localField': 'uploadMusic', 
      'foreignField': '_id', 
      'as': 'uploadMusic'
    }
  }, {
    '$unwind': {
      'path': '$uploadMusic', 
      'preserveNullAndEmptyArrays': true
    }
  },{
    '$set': {
      'uploadMusic.uploader.username': '$username'
    }
  }, {
    '$group': {
      '_id': '$_id', 
      'username': {
        '$first': '$username'
      }, 
      'password': {
        '$first': '$password'
      }, 
      'email': {
        '$first': '$email'
      }, 
      'uploadMusic': {
        '$push': '$uploadMusic'
      }, 
      'isAdmin': {
        '$first': '$isAdmin'
      }
    }
  }
])

module.exports = { findUserLikedSongs, findUserUploadedSongs  }