
const findNewUploadedCoveredSongs = () => ([
  {
    '$lookup': {
      'from': 'users', 
      'localField': 'uploader._id', 
      'foreignField': '_id', 
      'as': 'uploader'
    }
  }, {
    '$unwind': {
      'path': '$uploader', 
      'preserveNullAndEmptyArrays': true
    }
  }, {
    '$sort': {
      '_id': -1
    }
  }, {
    '$limit': 5
  }
])

module.exports = { findNewUploadedCoveredSongs }