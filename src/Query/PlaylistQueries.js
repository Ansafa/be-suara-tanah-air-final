const mongoose = require('mongoose');

const findPlaylistSongs = (playlistId) => ([
  {
    $match: {
      _id: new mongoose.Types.ObjectId(playlistId)
    }
  },
  {
    '$unwind': {
      'path': '$playlistSongs', 
      'preserveNullAndEmptyArrays': true
    }
  }, {
    '$lookup': {
      'from': 'songs', 
      'localField': 'playlistSongs', 
      'foreignField': '_id', 
      'as': 'playlistSongs'
    }
  }, {
    '$unwind': {
      'path': '$playlistSongs', 
      'preserveNullAndEmptyArrays': true
    }
  }, {
    '$group': {
      '_id': '$_id', 
      'title': {
        '$first': '$title'
      }, 
      'imgPath': {
        '$first': '$imgPath'
      }, 
      'playlistSongs': {
        '$push': '$playlistSongs'
      }, 
      'creator': {
        '$first': '$creator'
      }
    }
  }
])

module.exports = { findPlaylistSongs  }