const {
  create, getByid, updateById, removeSongFromPlaylist,
  updateImagePathName, addToPlaylist, deletePlaylist,
  fetchPlaylistSongs, upload
} = require('../service/PlaylistService')
const multer = require('multer');

class PlaylistController {
  static createPlaylist = async (req, res) => {
    try {
      const createdPlaylist = await create(req.body)
      res.status(201).json({ msg: 'Playlist created', createdPlaylist })
    } catch (error) {

    }
  }

  static getPlaylistById = async (req, res) => {
    try {
      const playlist = await getByid(req.params.id)
      res.status(200).json(playlist)
    } catch (error) {

    }
  }

  static updatePlaylist = async (req, res) => {
    try {
      const playlistId = req.params.id
      const payload = req.body
      const updatedPlaylist = await updateById(playlistId, payload)
      res.status(201).json({ msg: 'Playlist updated', updatedPlaylist })
    } catch (error) {
      res.status(400).json({ msg: 'Playlist title must not be empty' })
    }
  }

  static deleteSongFromPlaylist = async (req, res) => {
    const playlist = await removeSongFromPlaylist(req.params)
    try {
      res.status(201).json({ msg: 'song deleted', playlist })
    } catch (error) {
      res.status(400).json({ error })
    }
  }

  static playlistUploadImage = (req, res) => {
    upload(req, res, function (err) {
      if (err instanceof multer.MulterError) {
        return res.status(500).json(err)
      } else if (err) {
        return res.status(500).json(err)
      }
      return res.status(200).send(req.file)

    })
  }

  static updatePlaylistImgPath = async (req, res) => {
    try {
      const param = req.body;
      const imgpath = await updateImagePathName(param.playlistId, param.imageName);
      return res.status(200).send(imgpath)
    } catch (error) {
      res.status(400).json({ error })
    }
  }

  static addSongToPlaylist = async (req, res) => {
    try {
      const playlist = await addToPlaylist(req.params, req.body)
      res.status(201).json({ msg: 'playlist updated', playlist })
    } catch (error) {
      res.status(400).json({ error })
    }
  }

  static deleteUserPlaylist = async (req, res) => {
    try {
      const playlist = await deletePlaylist(req.params)
      res.status(201).json({ msg: 'playlist deleted', playlist })
    } catch (error) {

    }
  }

  static getPlaylistSongs = async (req, res) => {
    const { playlistId } = req.params
    try {
      const playlistSongs = await fetchPlaylistSongs(playlistId)
      res.status(200).json(playlistSongs)
    } catch (error) {

    }
  }
}





module.exports = PlaylistController;