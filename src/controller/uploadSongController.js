const {
  uploadSong, upload, updateUserAndCoverSongByPath,
  deleteAndUpdateUploadSong, addLike, removeLike,
  fetchAll, deleteUploadSongAdmin, reportUploadSong,
  fetchUploadSongsWithLimitedReports,
  fetchNewUploadedCoveredSongs
} = require('../service/uploadSongService');
const UploadSong = require('../model/uploadSong');
const multer = require('multer');

class UploadSongController {
  static uploadFile = async (req, res) => {
   
    upload(req, res, function (err) {
      if (err instanceof multer.MulterError) {
        return res.status(500).json(err)
      } else if (err) {
        return res.status(500).json(err)
      }
      return res.status(200).send(req.file)

    })
  };

  static postUploadSong = async (req, res) => {
    const param = req.body;
    const newSong = await uploadSong(param);
    const uploadedSong = new UploadSong(newSong);
    const insertedSong = await uploadedSong.save();
    await updateUserAndCoverSongByPath(param.uploader, param.songCover);
    try {
      res.status(201).json(insertedSong)
    } catch (error) {
      res.status(400).json({ msg: error })
    }
  };

  static deleteUploadSong = async (req, res) => {
    const param = req.params.id;
    try {
      await deleteAndUpdateUploadSong(param);
      res.status(200).json({ msg: deleted })
    } catch (error) {
      res.status(400).json({ msg: error })
    }

  }

  static adminDeleteUploadSong = async (req, res) => {
    try {
      const songId = req.params.songId
      const deletedSong = await deleteUploadSongAdmin(songId)
      res.status(200).json({ msg: 'delete success', deletedSong })
    } catch (error) {

    }
  }

  static getAllSongUpload = async (req, res) => {
    const songs = await fetchAll();
    res.status(200).json({ msg: 'Fetch success', songs })
  }

  static addLikeUploadSong = async (req, res) => {
    try {
      const likedSong = await addLike(req.body)
      res.status(201).json(likedSong)
    } catch (error) {
      res.send(error)
    }
  }

  static unlikeUploadSong = async (req, res) => {
    try {
      const deletedSong = await removeLike(req.params)
      res.status(200).json({ msg: 'song deleted', deletedSong })
    } catch (error) {
      res.send(error)
    }
  }

  static addReportUploadSong = async (req, res) => {
    const songId = req.params.songId
    const payload = req.body
    try {
      const reportedSong = await reportUploadSong(songId, payload)
      res.status(201).json({ msg: 'song reported', reportedSong })
    } catch (error) {
      res.send(error)
    }
  }

  static getUploadSongsWithLimitedReports = async (req, res) => {
    const songs = await fetchUploadSongsWithLimitedReports();
    res.status(200).json(songs)
  }

  static getNewUploadedCoveredSong = async (req, res) => {
    const songs = await fetchNewUploadedCoveredSongs();
    res.status(200).json(songs)
  }
}

module.exports = UploadSongController;
