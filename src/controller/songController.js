
const { 
  addLike, removeLike, fetchSongsCoverByOriginalSong, fetchNewUploadedSongList, fetchPopularSongs, fetchSong, fetchSongById,
  downloadSongByid,
  getSongPath
} = require('../service/SongService')

class SongController {

  static getSong = async (req, res) => {
    const songs = await fetchSong(req.body.filters)
    try {
      res.status(200).json({ success: true, songs })
    } catch (error) {
      res.status(400).json({ success: false, err })
    } 
  }

  static getSongDetail = async (req, res) => {
    const song = await fetchSongById(req.params.id)
    try {
      res.status(200).json(song)
    } catch (error) {
      
    }
  }

  static addLikeSong = async (req, res) => {
    try {
      const likedSong = await addLike(req.body)
      res.status(201).json(likedSong)
    } catch (error) {
      res.send(error)
    }
  }

  static unlikeSong = async (req, res) => {
    try {
      const deletedSong = await removeLike(req.params)
      res.status(200).json({ msg: 'song deleted', deletedSong })
    } catch (error) {
      res.send(error)
    }
  }

  static downloadSong = async (req, res) => {
    const songPath = await getSongPath(req.params.id);
    try {
      res.download(songPath);
    } catch (error) {
      
    }
   
  };

  static getSongsCoverByOriginalSong = async (req, res) => {
    const songId = req.params.songId
    try {
      const songs = await fetchSongsCoverByOriginalSong(songId)
      res.status(200).json(songs)
    } catch (error) {

    }
  }

  static getNewUploadedSongList = async (req, res) => {
    const songs = await fetchNewUploadedSongList();
    res.status(200).json(songs)
  }

  static getPopularSongs = async (req, res) => {
    const songs = await fetchPopularSongs();
    try {
      res.status(200).json(songs)
    } catch (error) {
      
    }
  }
}

module.exports = SongController;
