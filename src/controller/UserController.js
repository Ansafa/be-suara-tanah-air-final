const User = require('../model/User');
const { fetchUserLikedSong, fetchUsersUploadSong, hashUserPassword, fetchUserById, updateUserByid, validateDuplicateEmail } = require('../service/UserService');

class UserController {
  static registerUser = async (req, res) => {
    const validateEmail = await validateDuplicateEmail(req.body.email);
    if (validateEmail === false) {
      res.status(200).json({ errorMessage: "Email sudah terdaftar"})
    } else {
      const user = new User(req.body);
      await user.save();
      await hashUserPassword(user.id, user.password);
      try {
        res.status(200).json(user)
      } catch (error) {
        res.status(400).json({ msg: error })
      }
    }
  };

  static getUserById = async (req, res) => {
    const user = await fetchUserById(req.params.userId)
    try {
      return res.status(200).json(user)
    } catch (error) {
      return res.status(404).json({ error })
    }
  }

  static updateUser = async (req, res) => {
    const user = await updateUserByid(req.body)
    try {
      res.status(201).json(user)
    } catch (error) {
      res.status(404).json(error)
    }
  }

  static getUserLikedSongs = async (req, res) => {
    const { userId } = req.params
    try {
      const likedSongs = await fetchUserLikedSong(userId);
      res.status(200).json(likedSongs)
    } catch (error) {

    }
  }

  static getUsersUploadSong = async (req, res) => {
    const { userId } = req.params
    try {
      const uploadedSongs = await fetchUsersUploadSong(userId)
      res.status(200).json(uploadedSongs)
    } catch (error) {

    }
  }
}

module.exports = UserController;
