const requestBodyValidator = schema => async (req, res, next) => {
  try {
    const { body } = req;
    await schema.validateAsync(body);
    return next();
  } catch (error) {
    const errorBody = {
      errorMessage: "Mohon untuk mengisi semua field yang telah disediakan"
    };
    return res.status(200).json(errorBody);
  }
};

module.exports = requestBodyValidator;
