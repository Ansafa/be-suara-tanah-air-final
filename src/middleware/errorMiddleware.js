// eslint-disable-next-line no-unused-vars
const errorMiddleware = (err, req, res, next) => {
  const errorCode = err.statusCode || 500;
  return res.status(errorCode).json({
    statusCode: errorCode,
    message: err.message
  });
};

module.exports = errorMiddleware;
