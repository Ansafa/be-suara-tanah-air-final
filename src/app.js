const express = require('express');
const routes = require('./routes');
const connect = require('./database');
const app = express();

const cors = require('cors');
app.use(express.static('public'))
app.use(cors());

connect();
app.use(express.json());
app.use(routes);


module.exports = app;
