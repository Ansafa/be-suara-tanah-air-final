const Joi = require('@hapi/joi');

const userSchema = Joi.object({
  username: Joi.string()
    .required(),

  password: Joi.string()
    .required(),

  email: Joi.string()
    .email({ tlds: { allow: ['com', 'net'] } })
});

module.exports = userSchema;
