const { array } = require('@hapi/joi');
const mongoose = require('mongoose');

const { Schema } = mongoose;

const userSchema = new Schema({
  username: { type: String, required: true },
  password: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  likedMusic: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'songs'
  }],
  likedUploadMusic: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'uploadSongs'
  }],
  uploadMusic: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'uploadSongs'
  }],
  playlists: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'playlists'
  }],
  isAdmin: { type: Boolean, default: false}
});

userSchema.set('toJSON', {
  transform: (_, result) => {
    const { _id: id, __v, ...otherFields } = result;
    return {
      id,
      ...otherFields
    };
  }
});

const User = mongoose.model('User', userSchema);

module.exports = User;
