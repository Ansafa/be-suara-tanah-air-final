const mongoose = require('mongoose')

const uploaderSchema = mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    email: {
        type: String,
        required: true
    }
})

const songCoverSchema = mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title: {
        type: String,
        required: true
    },
    origin: {
        type: String,
        required: true
    }
})

const uploadSongSchema = mongoose.Schema({
    uploader: uploaderSchema,
    songCover: songCoverSchema,
    songName: {
        type: String,
        required: true
    },
    path: {
        type: String,
        required: true
    },
    likes: {
        type: Array,
        default: []
    },
    reports: {
        type: Array,
        default: []
    }
});

const UploadSong = mongoose.model('uploadSongs', uploadSongSchema);

module.exports = UploadSong;
