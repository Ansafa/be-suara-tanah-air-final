const Song = require('../model/song');
const { addLikedSongToUser, removeLikedSongFromUser } = require('./UserService')
const { findSongsCoverByOriginalSong, findPopularSongs } = require('../Query/SongQueries') 

class SongService {

    static fetchSong = async (filters) => {
         let findArgs = {};
    
        for (let key in filters) {
        if (filters[key].length > 0) {
            findArgs[key] = filters[key];
            }
        }

        const songs = await Song.find(findArgs)
        return songs
    }

    static fetchSongById = async (songId) => {
        const song = await Song.findById(songId)
        return song
    }

    static addLike = async (payload) => {
        try {
            const { userId, songData } = payload;
            const { _id } = songData;
            const song = await Song.findById(_id);
            const currLikes = song.likes
            currLikes.push(userId);
            await addLikedSongToUser(userId, _id);
            song.likes = currLikes;
            song.save()
            return song;
        } catch (error) {
            throw (error)
        }
    }
    
    static removeLike = async (params) => {
        const { userId, songId } = params;
        try {
            await removeLikedSongFromUser(userId, songId)
            const likedSong = await Song.findById(songId);
            const { likes } = likedSong;
            const newLikes = likes.filter(id => id !== userId)
            await Song.findByIdAndUpdate(songId, { likes: newLikes })
            return songId;
        } catch (error) {
            throw (error)
        }
    }
    
    static addUploadSongToSongs = async (songId, uploadSongId) => {
        await Song.updateOne(
            { _id: songId },
            { $push: { uploadMusic: uploadSongId } }
        )
    }
    
    static removeUploadSongFromSongs = async (songId, uploadSongId) => {
        await Song.updateOne(
            { _id: songId },
            { $pull: { uploadMusic: uploadSongId } }
        )
    }
    
    static adminRemoveUploadSongFromSong = async (songCover, songId) => {
        await Song.updateOne(
            { _id: songCover },
            { $pull: { uploadMusic: songId } }
        )
    }
    
    static fetchSongsCoverByOriginalSong = async (songId) => {
        const query = findSongsCoverByOriginalSong(songId)
        return Song.aggregate(query);
    }

    static fetchNewUploadedSongList = () => {
        try {
            const songs = Song.find().sort({ _id: -1 }).limit(10)
            return songs;
        } catch (error) {
        
        }
    }

    static fetchPopularSongs = () => {
       const query = findPopularSongs()
       return Song.aggregate(query)
    }
    
    static getSongPath = async (songId) => {
        const song = await Song.findById(songId);
        try {
            const songPath = song.path;
            const filePath = 'public//' + songPath.substring(22, songPath.length);
            return filePath;
        } catch (error) {
            
        }
    }

}

module.exports = SongService;